Tools Overview
==============

GTTN Profile provides some tools for managing and using the data it holds. Here is a brief overview of the tools it provides and their functionality:

Service Providers Directory
---------------------------
The Service Providers Directory contains a listing of all the registered GTTN organizations within the GTTN Database. It shows a brief summary of each organization, including the organization name, website, supported analysis species, methods, products, and regions. The table can be filtered by species, method, product, or region. Clicking on the name of any organization within the Service Providers Directory will redirect the user to an organization detail view, where they can browse all of the information about the organization that is held in the database. For more information about the data found in the organization detail view, see `Organization Metadata`_.

Service Request Form
--------------------
The Service Request Form provides an interface where users can obtain the contact information of organizations that are qualified to run analysis on a sample in question. Users will select the type of question they need answered, details about the sample, time constraints for the analysis, regional constraints, certification requirements, etc, and in return will receive a list of contacts for organizations that meet all of the specified requirements.

.. _Organization Metadata: details/organization_metadata.html
