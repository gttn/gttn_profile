Introduction
============

The GTTN Profile module is a collaboration between the University of Connecticut's Plant Computational Genomics lab and the Global Timber Tracking Network (GTTN). It allows for the management of users and organizations within the GTTN network.

For users, GTTN Profile keeps track of organization membership. For organizations, GTTN Profile keeps track of sample analysis competencies.

The module is still in development, but is expected to be completed during the summer of 2019.
