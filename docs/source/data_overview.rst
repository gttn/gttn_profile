Data Overview
=============

GTTN Profile keeps track of two different data types: users and organizations. Below are some more details about what data is actually collected and tracked:

User Information
----------------
GTTN Profile collects and tracks very minimal information about users - user information is limited to email address, name, and organization membership. Upon registering to become part of the GTTN network, users will be asked to provide their name and email address, as well as select the organization(s) they are part of.

.. image:: ../../images/user_info.png

Organization Information
------------------------
GTTN Profile collects more information about organizations than it does for users. When registering, an organization must provide general information such as address, website, and primary contact information, as well as analysis competencies, certifications, partner organizations, legal restrictions, etc. For more information, please refer to `Organization Metadata`_.

.. image:: ../../images/organization_info.png

.. _Organization Metadata: details/organization_metadata.html

