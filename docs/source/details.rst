GTTN Profile Details
====================

.. toctree::
    :maxdepth: 1
    :caption: Table of Contents
    :glob:
    
    Organization Metadata <details/organization_metadata>

This section contains more detailed information about various aspects of the GTTN Profile Module.
