<?php

function gttn_profile_sample_request($form, &$form_state){

  //Create a fieldset/fieldgroup
  $form['search_welcome'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#prefix' => '<div class="fieldset"><span class="fieldset-legend"><span class="fieldset-legend-prefix element-invisible">Show</span>Search samples<span class="summary"></span></span>',
  );
  $prefix_text = "<div>Welcome to GTTN search Samples!<br><br>If you would like to submit your data, you can go to <b>'Submit Reference Data'</b> tab on GTTN front page<br>To find appropriate Samples metadata, please define your search criterias below:</div>";
  //$form['group_welcome']['#prefix'] = $prefix_text;
  $form['species'] = array(
    '#type' => 'textfield',
    '#title' => t('Species (autocomplete):'),
    '#autocomplete_path' => 'gttn_profile_species/autocomplete'
  );

  //country
  $country_cvt = chado_get_cvterm(array(
    'cv_id' => array(
      'name' => 'tripal_contact',
    ),
    'name' => 'Country',
    'is_obsolete' => 0,
  ))->cvterm_id;

  $countries_query = db_select('chado.stockprop', 'm')
    ->distinct()
    ->fields('m', array('value'))
    ->condition('type_id',$country_cvt)
    ->execute();

  $form['countries'] = array(
    '#type' => 'select',
    '#title' => t('Country:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($cntry = $countries_query->fetchObject()))
    $form['countries']['#options'][$cntry->value] = $cntry->value;

  //Tissue
  $tissue_cvt = chado_get_cvterm(array(
    'name' => 'Tissue',
    'cv_id' => array(
      'name' => 'ncit',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;

  $tissues_query = db_select('chado.stockprop', 'm')
    ->distinct()
    ->fields('m', array('value'))
    ->condition('type_id',$tissue_cvt)
    ->execute();

  $form['tissues'] = array(
    '#type' => 'select',
    '#title' => t('Tissue:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($tissue = $tissues_query->fetchObject()))
    $form['tissues']['#options'][$tissue->value] = $tissue->value;
  /*
  $products_query = db_select('gttn_profile_product', 'p')
    ->distinct()
    ->fields('p', array('product_id', 'name'))
    ->execute();

  $form['products'] = array(
    '#type' => 'select',
    '#title' => t('Products:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($product = $products_query->fetchObject()))
    $form['products']['#options'][$product->product_id] = $product->name;
  */
  //type
  $type_cvt = chado_get_cvterm(array(
    'name' => 'sample type',
    'is_obsolete' => 0,
  ))->cvterm_id;

  $types_query = db_select('chado.stockprop', 'm')
    ->distinct()
    ->fields('m', array('value'))
    ->condition('type_id',$type_cvt)
    ->execute();

  $form['types'] = array(
    '#type' => 'select',
    '#title' => t('Type:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($type = $types_query->fetchObject()))
    $form['types']['#options'][$type->value] = $type->value;
  //storage
  $storage_cvt = chado_get_cvterm(array(
    'name' => 'storage location',
    'is_obsolete' => 0,
  ))->cvterm_id;
  $storages_query = db_select('chado.stockprop', 'm')
    ->distinct()
    ->fields('m', array('value'))
    ->condition('type_id',$storage_cvt)
    ->execute();

  $form['storages'] = array(
    '#type' => 'select',
    '#title' => t('Storage:'),
    '#options' => array(
      0 => '- Select -',
    ),
  );

  while (($storage = $storages_query->fetchObject()))
    $form['storages']['#options'][$storage->value] = $storage->value;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['search_welcome']['#prefix'] .= $prefix_text;
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  drupal_add_css(drupal_get_path('module', 'gttn_profile') . GTTN_PROFILE_CSS_PATH);
  return $form;
}

function gttn_profile_sample_request_validate(&$form, &$form_state){
  if ($form_state['submitted'] == '1'){

  }
}

function gttn_profile_sample_request_submit($form, &$form_state){
  $samples = gttn_profile_sample_filter($form, $form_state);
  drupal_goto('sample_candidates', array(
    'query' => array('samples' => implode(';', $samples))
  ));
}

