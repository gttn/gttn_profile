<?php

/**
 * @file
 */
define("GTTN_PROFILE_CSS_PATH", '/css/style.css');

require_once 'includes/create_record.inc';
require_once 'includes/defaults.inc';
require_once 'includes/organization.inc';
require_once 'includes/user.inc';
require_once 'includes/service.inc';
require_once 'includes/sample.inc';
require_once 'includes/refdata.inc';
require_once 'includes/efi.inc';
require_once 'ajax/organization_ajax.php';

/**
 * Implements hook_menu()
 *
 * @return array Array of menu items.
 */
function gttn_profile_menu() {
  $items = array();

  // Primary Contact autocomplete path.
  $items['gttn_profile_contact/autocomplete'] = array(
    'title' => 'Autocomplete for Primary Contact',
    'page callback' => 'gttn_profile_contact_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'ajax/gttn_profile_ajax.php',
  );

  // Species autocomplete path.
  $items['gttn_profile_species/autocomplete'] = array(
    'title' => 'Autocomplete for Species',
    'page callback' => 'gttn_profile_species_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
    'file' => 'ajax/gttn_profile_ajax.php',
  );

  // Browse/search organizations.
  $items['organization/directory'] = array(
    'title' => 'Browse/Search Organizations',
    'page callback' => 'gttn_profile_organization_display',
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('browse_organization'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['organization/add'] = array(
    'title' => 'Add Organization',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gttn_profile_organization_add'),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('add_organization'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'forms/organization.php',
  );

  $items['organization/edit/%'] = array(
    'title' => 'Edit Organization',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gttn_profile_organization_edit', 2),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('edit_organization', 2),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'forms/organization.php',
  );

  $items['organization/members/%'] = array(
    'title' => 'Organization Members',
    'page callback' => 'gttn_profile_organization_members',
    'page arguments' => array(2),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('browse_organization_members'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['organization/%'] = array(
    'title' => 'Organization Details',
    'page callback' => 'gttn_profile_organization_details',
    'page arguments' => array(1),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('browse_organization_details', 1),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['organization_member_approve/%/%'] = array(
    'title' => 'Approve Organization member',
    'page callback' => 'gttn_profile_organization_approve_member',
    'page arguments' => array(1, 2),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['service_request'] = array(
    'title' => 'Service Request',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gttn_profile_service_request'),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('service_request'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'forms/service.php',
  );

  $items['service_candidates'] = array(
    'title' => 'Service Candidates',
    'page callback' => 'gttn_profile_service_candidates',
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('service_request'),
    'type' => MENU_NORMAL_ITEM,
  );

  // Browse/search samples.
  $items['sample_request'] = array(
    'title' => 'Search Sample Data',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gttn_profile_sample_request'),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('sample_request'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'forms/sample.php',
  );

  $items['sample_candidates'] = array(
    'title' => 'Samples Candidates',
    'page callback' => 'gttn_profile_sample_candidates',
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('sample_request'),
    'type' => MENU_NORMAL_ITEM,
  );

  // Browse/search reference data.
  $items['refdata_request'] = array(
    'title' => 'Search Reference Data',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gttn_profile_refdata_request'),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('refdata_request'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'forms/refdata.php',
  );
    
  $items['refdata_candidates'] = array(
    'title' => 'Reference Data Candidates',
    'page callback' => 'gttn_profile_refdata_candidates',
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('refdata_request'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/config/content/gttn_profile'] = array(
    'title' => 'GTTN Profile Settings',
    'description' => 'Configuration for GTTN Profile module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gttn_profile_admin_settings'),
    'access callback' => 'gttn_profile_access',
    'access arguments' => array('administer_gttn_profile'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'admin/config.php',
  );

  return $items;
}

/**
 * Implements hook_permission().
 *
 * This function creates custom permissions for the gttn_profile module that will
 * be checked by the user_access() function.
 */
function gttn_profile_permission() {
  $perms = array();

  $perms['administer_gttn_profile'] = array(
    'title' => t('Administer GTTN Profile'),
    'description' => t('Allows users to administer the GTTN Profile module'),
  );

  $perms['browse_organization'] = array(
    'title' => t('Browse/Search SPD'),
    'description' => t('Enables users to browse and search the Service Provider Directory.'),
  );

  $perms['add_organization'] = array(
    'title' => t('Add Organization'),
    'description' => t('Enables users to add organizations to the Service Provider Directory. This permission will also allow users to become primary contact members.'),
  );

  $perms['edit_organization'] = array(
    'title' => t('Edit Organization'),
    'description' => t('Enables users to edit organizations in the Service Provider Directory. This permission will only allow the primary contact of each organization to edit their own organization.'),
  );

  $perms['browse_organization_members'] = array(
    'title' => t('Browse Organization Members'),
    'description' => t('Enables users to browse the members of a specific organization.'),
  );

  $perms['browse_organization_details'] = array(
    'title' => t('Browse Organization Details'),
    'description' => t('Enables users to browse the details of a specific organization.'),
  );

  $perms['approve_organization_members'] = array(
    'title' => t('Approve Organization Members'),
    'description' => t('Enables users to approve new members of an organization. This permission will only allow the primary contact of each organization to edit their own organization.'),
  );

  $perms['service_request'] = array(
    'title' => t('Submit Service Request'),
    'description' => t('Enables users to submit a service request.'),
  );

  $perms['sample_request'] = array(
    'title' => t('Submit Sample Request'),
    'description' => t('Enables users to submit a sample request.')
  );

  $perms['refdata_request'] = array(
    'title' => t('Submit Reference Data Request'),
    'description' => t('Enables users to submit a reference data request.')
  );

  return $perms;
}

/**
 * This function adds some additional functionality to certain gttn_profile
 * permissions. It will first check the permission type against the user_access()
 * function, then perform some additional checks where necessary.
 *
 * @param string $type
 *   The type of permission to be granted.
 * @param int $param
 *   Extra parameters certain permissions need.
 */
function gttn_profile_access($type, $param = NULL) {
  if (!user_access($type)) {
    return FALSE;
  }

  global $user;

  if ($user->uid == 1) {
    return TRUE;
  }

  switch ($type) {
    case 'browse_organization_details':
      if (gttn_profile_access('administer_gttn_profile')) {
        return TRUE;
      }
      if (!empty($param)) {
        $user = user_load($user->uid);
        foreach ($user->organizations as $org) {
          if ($org == $param and $user->organization_status[$org]) {
            return TRUE;
          }
        }
      }
      return FALSE;

    case 'edit_organization':
    case 'approve_organization_members':
      if (gttn_profile_access('administer_gttn_profile')) {
        return TRUE;
      }

      if ($user->uid == 0) {
        $destination = drupal_get_destination();
        drupal_goto('user/login', array('query' => $destination));
      }
      $and = db_and()
        ->condition('uid', $user->uid)
        ->condition('organization_id', $param)
        ->condition('status', 1)
        ->condition('is_primary_contact', TRUE);
      $query = db_select('gttn_profile_organization_members', 'm')
        ->fields('m', array())
        ->condition($and)
        ->execute();
      if (!empty($query->fetchObject())) {
        return TRUE;
      }
      break;

    case 'sample_request':
      return TRUE;
      break;
    case 'refdata_request':
      return TRUE;
      break;

    default:
      return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_form_alter().
 *
 * Used here to alter the user and organization registration forms.
 *
 * @param array $form
 *   The form to be altered.
 * @param array $form_state
 *   The state of the form to be altered.
 * @param string $form_id
 *   The form_id of the form to be altered.
 */
function gttn_profile_form_alter(&$form, &$form_state, $form_id) {

  switch ($form['#form_id']) {

    // Make changes to the user registration form.
    case 'user_register_form':
    case 'user_profile_form':
      require_once 'forms/user.php';
      gttn_profile_user_form($form, $form_state, $form_id);
      $form['#validate'][] = 'gttn_profile_user_validate';
      break;

    default:
      break;
  }
}

/**
 *
 */
function gttn_profile_form_user_login_alter(&$form, $form_state) {
  $form['#validate'] = array('user_login_name_validate', 'user_login_authenticate_validate', 'gttn_profile_authentication_login_validate', 'user_login_final_validate');
}

/**
 *
 */
function gttn_profile_authentication_login_validate($form, &$form_state) {
  $username = $form_state['values']['name'];
  $response = gttn_profile_authentication_check_user($username, $form_state['values']['pass']);
  if ($response != FALSE) {

    $account = user_external_load($username);
    if (!$account) {
      $org_id = $response->org;
      $org_arr = array();
      $org = gttn_profile_efi_load_organization($org_id, $username, $form_state['values']['pass']);
      if (!empty($org)) {
        $org_arr[$org->organization_id] = TRUE;
      }

      // Register this new user.
      $userinfo = array(
        'name' => $username,
        'mail' => $response->mail,
        'full_name' => $response->full_name,
        'pass' => user_password(),
        'init' => $username,
        'status' => 1,
        'access' => REQUEST_TIME,
        'org' => $org_arr,
        'efi' => TRUE,
      );
      $account = user_save(drupal_anonymous_user(), $userinfo);

      if (!$account) {
        drupal_set_message(t("Error saving user account."), 'error');
      }
      else {
        user_set_authmaps($account, array("authname_gttn_profile" => $username));
      }
    }

    // Log user in.
    if ($account) {
      $form_state['uid'] = $account->uid;
      user_login_submit(array(), $form_state);
    }
  }
}

/**
 *
 */
function gttn_profile_user_validate(&$form, &$form_state) {
  $form_state['values']['name'] = $form_state['values']['mail'];
}

/**
 * Implements hook_user_presave().
 *
 * This function copies the contents of the 'mail' field to the 'name' field
 * because we want them to be the same. It is called whenever a user object is
 * about to be saved or updated.
 *
 * @param array $edit
 *   Form values submitted by user.
 * @param stdObject $account
 *   User object.
 * @param stdObject $category
 *   Category of user information being edited.
 */
function gttn_profile_user_presave(&$edit, $account, $category) {
  // Assign email address to username.
  $edit['name'] = $edit['mail'];
}

/**
 * Implements hook_user_insert().
 *
 * This function simply calls gttn_profile_user_chado_update(), which updates
 * information about a user when their information is saved for the first time
 * or updated. It is called after a new user object was saved.
 *
 * @param array $edit
 *   The edit or update form that was just submitted.
 * @param object $account
 *   The existing user account object.
 * @param string $category
 *   The category of user information being changed.
 */
function gttn_profile_user_insert(&$edit, $account, $category) {
  gttn_profile_user_chado_update($edit, $account);
}

/**
 * Implements hook_user_update().
 *
 * This function simply calls gttn_profile_user_chado_update(), which updates
 * information about a user when their information is saved for the first time
 * or updated. It is called after an existing user object is updated.
 *
 * @param array $edit
 *   The edit or update form that was just submitted.
 * @param object $account
 *   The existing user account object.
 * @param string $category
 *   The category of user information being changed.
 */
function gttn_profile_user_update(&$edit, $account, $category) {
  gttn_profile_user_chado_update($edit, $account);
}

/**
 * Implements hook_mail().
 *
 * @param string $key
 *   The identifier of the mail.
 * @param array $message
 *   Information about the message to be sent.
 * @param array $params
 *   Additional custom parameters.
 */
function gttn_profile_mail($key, &$message, $params) {
  global $base_url;

  if (isset($params['subject'])) {
    $message['subject'] = $params['subject'];
  }
  if (isset($params['body'])) {
    $message['body'][] = $params['body'];
  }
  if (!empty($params['headers'])) {
    $message['headers'] += $params['headers'];
  }

  switch ($key) {
    case 'new_member':
      $member_name = $params['user']->chado_record->name;

      $org_name = db_select('gttn_profile_organization', 'o')
        ->fields('o', array('name'))
        ->condition('organization_id', $params['org_id'])
        ->execute()->fetchObject()->name;

      $message['body'][] = "Attention:<br><br>A user has claimed to be"
            . " a member of $org_name. The new user's name is {$member_name}"
            . ". If this person is not a member of your organization, then you n"
            . "eed not take any action. If the user is a member of your organiza"
            . "tion, then you will need to approve them <a href=\"$base_url/orga"
            . "nization_member_approve/{$params['user']->uid}/{$params['org_id']}"
            . "\">here</a>.";
      break;

    case 'remove_member':
      $member_name = $params['user']->chado_record->name;

      $org_name = db_select('gttn_profile_organization', 'o')
        ->fields('o', array('name'))
        ->condition('organization_id', $params['org_id'])
        ->execute()->fetchObject()->name;

      $message['body'][] = "Attention:<br><br>$member_name was previously "
            . "part of $org_name and has either removed themselves or had their "
            . "membership revoked. Please contact the GTTN site administrator if"
            . " you have any questions.";
      break;

    default:
      break;
  }
}
