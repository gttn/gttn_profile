<?php

/**
 * @file
 */

/**
 * This function creates the autocomplete options for the organization primary
 * contact field.
 *
 * @param string $string
 *   The text the user has typed so far.
 */
function gttn_profile_contact_autocomplete($string) {
  $matches = array();

  $result = chado_select_record('contact', array('name'), array(
    'name' => array(
      'data' => $string,
      'op' => '~*',
    ),
    'type_id' => array(
      'name' => 'Person',
      'cv_id' => array(
        'name' => 'tripal_contact',
      ),
    ),
  ));

  // Populate options array.
  foreach ($result as $row) {
    $matches[$row->name] = check_plain($row->name);
  }

  // Return options array.
  drupal_json_output($matches);
}

/**
 *
 */
function gttn_profile_species_autocomplete($string) {
  $matches = array();

  $parts = explode(" ", $string);
  if (!isset($parts[1])) {
    $parts[1] = "";
  }

  $result = chado_select_record('organism', array('genus', 'species'), array(
    'genus' => array(
      'data' => $parts[0],
      'op' => '~*',
    ),
    'species' => array(
      'data' => $parts[1],
      'op' => '~*',
    ),
  ));

  foreach ($result as $row) {
    $matches[$row->genus . " " . $row->species] = check_plain($row->genus . " " . $row->species);
  }

  drupal_json_output($matches);
}
