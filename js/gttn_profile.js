jQuery(document).ready(function($) {
    var org_search_button = $('#gttn-org-search');
    
    org_search_button.click(function(){
        var params = [];
        if ($('#gttn-org-species')[0].value !== ""){
            params.push("species=" + $('#gttn-org-species')[0].value);
        }
        if ($('#gttn-org-method')[0].value !== ""){
            params.push("method=" + $('#gttn-org-method')[0].value);
        }
        if ($('#gttn-org-product')[0].value !== ""){
            params.push("product=" + $('#gttn-org-product')[0].value);
        }
        if ($('#gttn-org-region')[0].value !== ""){
            params.push("region=" + $('#gttn-org-region')[0].value);
        }
        
        window.location.href = window.location.pathname + "?" + params.join('&');
    });
    
    var add_species = $('input[name="Add Genus/Species"]');
    var remove_species = $('input[name="Remove Genus/Species"]');
    add_species.attr('type', 'button');
    remove_species.attr('type', 'button');
});
