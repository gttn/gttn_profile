<?php

function gttn_profile_refdata_filter($form, $form_state) {
    
    $refdatas = array();
    /*
    $and = db_and();
    if (!empty($form_state['values']['species'])){
        $species = explode(' ', $form_state['values']['species']);
        $species_and = db_and()
            ->condition('genus', $species[0])
            ->condition('species', $species[1]);
        
        $species_id = db_select('chado.organism', 'o')
            ->fields('o', array('organism_id'))
            ->condition($species_and)
            ->execute()->fetchObject()->organism_id;
        $and->condition('species_id', $species_id);
    }
    
    if (!empty($form_state['values']['methods']))
        $and->condition('method_id', $form_state['values']['methods']);
    if (!empty($form_state['values']['regions']))
        $and->condition('region_id', $form_state['values']['regions']);
    if (!empty($form_state['values']['products']))
        $and->condition('product_id', $form_state['values']['products']);
    
    $query = db_select('gttn_profile_support', 's')
        ->distinct();
    $query->fields('s', array('organization_id'));
    if ($and->count() > 0){
        $query->condition($and);
    }
    
    $results = $query->execute();
    while (($result = $results->fetchObject())){
        $orgs[] = $result->organization_id;
    }
    */
    return $refdatas;
}

function gttn_profile_refdata_candidates(){
    
    $per_page = 20;
    $rows = array();
    
    $refdatas = explode(';', $_GET['refdatas']);
    /*
    $or = db_or();
    foreach ($orgs as $org_id){
        $or->condition('organization_id', $org_id);
    }

    $query = db_select('gttn_profile_organization', 'o')
        ->fields('o', array('name', 'address_1', 'address_2', 'country', 'city', 'region', 'postal_code', 'website'))
        ->condition($or)
        ->execute();
    
    while (($org = $query->fetchObject())){
        $name = $org->name;
        $address = $org->address_1 . "<br>";
        if (!empty($org->address_2)){
            $address .= $org->address_2 . "<br>";
        }
        $address .= "{$org->city}, {$org->region}, {$org->country} {$org->postal_code}";
        $site = "<a target=\"blank\" href=\"http://{$org->website}\">{$org->website}</a>";
        
        $row = array($name, $address, $site);
        $rows[$name] = $row;
    }
    
    ksort($rows);
    */
    $current_page = pager_default_initialize(count($rows), $per_page);
    $chunks = array_chunk($rows, $per_page, TRUE);
    
    $vars = array(
      'header' => array(
        'InternalID', 'SampleID', 'Lab performed', 'Method','Format','Publication DOI','Data DOI','URL','Availability'
      ),
      'rows' => isset($chunks[$current_page]) ? $chunks[$current_page] : NULL,
      'attributes' => array(),
      'caption' => '',
      'colgroups' => NULL,
      'sticky' => FALSE,
      'empty' => ''
    );
    
    $output = theme_table($vars);

    $output = theme('pager', array('quantity', count($rows))) . $output;
    drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
    drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
    return $output;
}

