<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_organization_get_species($org_id) {

  $species_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('species_id'))
    ->condition('organization_id', $org_id)
    ->execute();
  $or = db_or();
  $condition = FALSE;
  while (($species_id = $species_id_query->fetchObject())) {
    $or->condition('organism_id', $species_id->species_id);
    $condition = TRUE;
  }
  if (!$condition) {
    return array();
  }
  $species_query = db_select('chado.organism', 'o')
    ->fields('o', array('organism_id', 'abbreviation', 'genus', 'species', 'common_name'))
    ->condition($or)
    ->execute();

  $species = array();
  while (($species_result = $species_query->fetchObject())) {
    $species[$species_result->organism_id] = $species_result;
  }
  return $species;
}

/**
 *
 */
function gttn_profile_organization_get_methods($org_id) {
  $method_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('method_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  $or = db_or();
  $condition = FALSE;
  while (($method_id = $method_id_query->fetchObject())) {
    $or->condition('method_id', $method_id->method_id);
    $condition = TRUE;
  }
  if (!$condition) {
    return array();
  }

  $method_query = db_select('gttn_profile_method', 'm')
    ->fields('m', array('method_id', 'name', 'description'))
    ->condition($or)
    ->execute();

  $methods = array();
  while (($method_record = $method_query->fetchObject())) {
    $methods[] = $method_record;
  }
  return $methods;
}

/**
 *
 */
function gttn_profile_organization_get_products($org_id) {
  $product_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('product_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  $or = db_or();
  $condition = FALSE;
  while (($product_id = $product_id_query->fetchObject())) {
    $or->condition('product_id', $product_id->product_id);
    $condition = TRUE;
  }
  if (!$condition) {
    return array();
  }

  $product_query = db_select('gttn_profile_product', 'p')
    ->fields('p', array('product_id', 'name', 'hs_code'))
    ->condition($or)
    ->execute();

  $products = array();
  while (($product_record = $product_query->fetchObject())) {
    $products[] = $product_record;
  }
  return $products;
}

/**
 *
 */
function gttn_profile_organization_get_regions($org_id) {
  $region_id_query = db_select('gttn_profile_support', 's')
    ->distinct()
    ->fields('s', array('region_id'))
    ->condition('organization_id', $org_id)
    ->execute();

  $or = db_or();
  $condition = FALSE;
  while (($region_id = $region_id_query->fetchObject())) {
    $or->condition('region_id', $region_id->region_id);
    $condition = TRUE;
  }
  if (!$condition) {
    return array();
  }

  $region_query = db_select('gttn_profile_region', 'p')
    ->fields('p', array('region_id', 'name'))
    ->condition($or)
    ->execute();

  $regions = array();
  while (($region_record = $region_query->fetchObject())) {
    $regions[] = $region_record;
  }
  return $regions;
}

/**
 *
 */
function gttn_profile_organization_get_members($org_id) {
  $members = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition('m.organization_id', $org_id)
    ->execute();

  $result = array();
  while (($member = $members->fetchObject())) {
    $result[] = user_load($member->uid);
  }
  return $result;
}

/**
 *
 */
function gttn_profile_organization_primary_contact($org_id) {
  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition('m.organization_id', $org_id)
    ->condition('m.is_primary_contact', TRUE)
    ->execute();

  $result = array();
  while (($contact = $query->fetchObject())) {
    $result[] = user_load($contact->uid);
  }
  return $result;
}

/**
 *
 */
function gttn_profile_organization_details($org_id) {

  global $base_url;
  global $user;
  $output = "";
  $org = db_select('gttn_profile_organization', 'o')
    ->fields('o')
    ->condition('organization_id', $org_id)
    ->execute()->fetchObject();

  // General Information.
  $output .= "<h2>General Information</h2>";
  $rows[] = array('Name', $org->name);
  $rows[] = array('Address Line 1', $org->address_1);
  if (!empty($org->address_2)) {
    $rows[] = array('Address Line 2', $org->address_2);
  }
  $rows[] = array('City', $org->city);
  if (!empty($org->region)) {
    $rows[] = array('State/Province', $org->region);
  }
  $rows[] = array('Country', $org->country);
  $rows[] = array('Website', "<a target=\"blank\" href=\"http://{$org->website}\">{$org->website}</a>");

  $vars = array(
    'header' => array(
      'Attribute', 'Value',
    ),
    'rows' => $rows,
    'attributes' => array('class' => array('view'), 'id' => 'gttn_profile_table_display'),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );
  $output .= theme_table($vars);

  // TODO: supported species/methods/products/regions.
  // Properties.
  $output .= "<h2>Properties</h2>";
  $property_query = db_select('gttn_profile_organization_property', 'op');
  $property_query->join('gttn_profile_property', 'p', 'p.property_id = op.property_id');
  $property_query->fields('op', array('value'))
    ->fields('p', array('name', 'description'))
    ->condition('organization_id', $org_id);
  $property_query = $property_query->execute();

  $rows = array();
  while (($property = $property_query->fetchObject())) {
    $name = $property->description;
    $value = empty($property->value) ? "Yes" : $property->value;

    $row = array($name, $value);
    $rows[$property->name] = $row;
  }

  $vars = array(
    'header' => array(
      'Property Name', 'Value',
    ),
    'rows' => $rows,
    'attributes' => array('class' => array('view'), 'id' => 'gttn_profile_table_display'),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );
  $output .= theme_table($vars);

  $output .= "<h2>Members</h2>";
  $rows = array();
  $members = gttn_profile_organization_get_members($org_id);
  $approved_img = "<img src='$base_url/misc/message-16-ok.png'>";
  $default_img = "<img src='$base_url/misc/message-16-warning.png'>";
  foreach ($members as $member) {
    $rows[] = array(
      $member->chado_record->name,
      $member->status ? $approved_img : $default_img,
      $member->organization_status[$org_id] ? $approved_img : $default_img,
    );
  }
  $vars = array(
    'header' => array(
      'Member Name',
      'Account Active',
      'Membership Verified',
    ),
    'rows' => $rows,
    'attributes' => array('class' => array('view'), 'id' => 'gttn_profile_table_display'),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );
  $output .= theme_table($vars);

  if (module_exists('gttn_tpps') and gttn_profile_is_primary_contact($org_id, $user->uid)) {
    $output .= "<h2>Reference Data Submissions</h2>";
    $rows = array();

    $query = db_select('gttn_tpps_submission', 's');
    $query->join('chado.dbxref', 'dbx', 'dbx.accession = s.accession');
    $query->join('chado.project_dbxref', 'pdbx', 'dbx.dbxref_id = pdbx.dbxref_id');
    $query->join('gttn_tpps_organization_project', 'op', 'op.project_id = pdbx.project_id');
    $query->fields('s', array('accession'));
    $query->condition('op.organization_id', $org_id);
    $query->condition('s.status', "Approved");
    $query = $query->execute();
    while (($result = $query->fetchObject())) {
      $accession = $result->accession;
      $state = gttn_tpps_load_submission($accession);
      $name = $state['data']['project']['name'];
      $species = array();
      foreach ($state['data']['organism'] as $info) {
        $species[] = "{$info['genus']} {$info['species']}";
      }
      $species = implode("<br>", $species);

      $data_types = array();
      foreach ($state['saved_values'][GTTN_PAGE_1]['data_type'] as $type) {
        if ($type) {
          $data_types[] = $type;
        }
      }
      $data_types = implode("<br>", $data_types);
      $rows[] = array(
        "<a href=\"$base_url/reference/submission/$accession\">$name</a>",
        $species,
        $data_types,
      );
    }

    $vars = array(
      'header' => array(
        'Submission Name',
        'Submission Species',
        'Submission Data Types',
      ),
      'rows' => $rows,
      'attributes' => array('class' => array('view'), 'id' => 'gttn_profile_table_display'),
      'caption' => '',
      'colgroups' => NULL,
      'sticky' => FALSE,
      'empty' => '',
    );
    $output .= theme_table($vars);
  }

  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  drupal_add_css(drupal_get_path('module', 'gttn_profile') . '/css/style.css');
  return $output;
}

/**
 *
 */
function gttn_profile_organization_members($org_id) {
  $per_page = 20;
  $org = db_select('gttn_profile_organization', 'o')
    ->fields('o')
    ->condition('organization_id', $org_id)
    ->execute()->fetchObject();

  $members = gttn_profile_organization_get_members($org_id);
  foreach ($members as $member) {
    $rows[$member->chado_record->name] = array(
      $member->chado_record->name,
    );
  }

  ksort($rows);

  $current_page = pager_default_initialize(count($rows), $per_page);
  $chunks = array_chunk($rows, $per_page, TRUE);
  $vars = array(
    'header' => array(
      'name',
    ),
    'rows' => isset($chunks[$current_page]) ? $chunks[$current_page] : NULL,
    'attributes' => array('class' => array('view'), 'id' => 'gttn_profile_table_display'),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );

  $output = theme_table($vars);

  $output = theme('pager', array('quantity', count($rows))) . $output;
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  drupal_add_css(drupal_get_path('module', 'gttn_profile') . '/css/style.css');
  return $output;
}

/**
 * This function displays organizations based on a search and provides a search form.
 *
 * @global string $base_url The base url of the site.
 * @global stdClass $user The user trying to browse organizations.
 *
 * @return string
 */
function gttn_profile_organization_display() {
  global $base_url, $user;

  $per_page = 20;

  $query = db_select('gttn_profile_organization', 'o');
  $query->leftJoin('gttn_profile_support', 's', 's.organization_id = o.organization_id');
  $query->addExpression('distinct(o.organization_id)', 'organization_id');
  $and = db_and();

  if (!empty($_GET['method'])) {
    $methods = explode(';', $_GET['method']);
    $method_or = db_or();
    foreach ($methods as $method) {
      $method_or->condition('name', '%' . db_like($method) . '%', 'LIKE');
    }
    $method_query = db_select('gttn_profile_method', 'm')
      ->fields('m', array('method_id'))
      ->condition($method_or)
      ->execute();
    $or = db_or();
    $empty_or = TRUE;
    while (($method = $method_query->fetchObject())) {
      $or->condition('method_id', $method->method_id);
      $empty_or = FALSE;
    }
    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('method_id', 1)
        ->condition('method_id', 0);
    }
  }
  if (!empty($_GET['product'])) {
    $products = explode(';', $_GET['product']);
    $product_or = db_or();
    foreach ($products as $product) {
      $product_or->condition('name', '%' . db_like($product) . '%', 'LIKE');
    }
    $product_query = db_select('gttn_profile_product', 'p')
      ->fields('p', array('product_id'))
      ->condition($product_or)
      ->execute();
    $or = db_or();
    $empty_or = TRUE;
    while (($product = $product_query->fetchObject())) {
      $or->condition('product_id', $product->product_id);
      $empty_or = FALSE;
    }
    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('product_id', 1)
        ->condition('product_id', 0);
    }
  }
  if (!empty($_GET['region'])) {
    $regions = explode(';', $_GET['region']);
    $region_or = db_or();
    foreach ($regions as $region) {
      $region_or->condition('name', '%' . db_like($region) . '%', 'LIKE');
    }
    $region_query = db_select('gttn_profile_region', 'p')
      ->fields('p', array('region_id'))
      ->condition($region_or)
      ->execute();
    $or = db_or();
    $empty_or = TRUE;
    while (($region = $region_query->fetchObject())) {
      $or->condition('region_id', $region->region_id);
      $empty_or = FALSE;
    }
    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('region_id', 1)
        ->condition('region_id', 0);
    }
  }
  if (!empty($_GET['species'])) {
    $species = explode(';', $_GET['species']);
    $or = db_or();
    $empty_or = TRUE;
    foreach ($species as $item) {
      $item = explode(' ', $item);
      $cond = db_and();
      if (!empty($item[1])) {
        $cond->condition('genus', '%' . db_like($item[0]) . '%', 'LIKE')
          ->condition('species', '%' . db_like($item[1]) . '%', 'LIKE');
      }
      else {
        $cond = db_or()
          ->condition('genus', '%' . db_like($item[0]) . '%', 'LIKE')
          ->condition('species', '%' . db_like($item[0]) . '%', 'LIKE');
      }
      $organism_query = db_select('chado.organism', 'o')
        ->fields('o', array('organism_id'))
        ->condition($cond)
        ->execute();
      while (($organism_id = $organism_query->fetchObject())) {
        $or->condition('species_id', $organism_id->organism_id);
        $empty_or = FALSE;
      }
    }

    if (!$empty_or) {
      $and->condition($or);
    }
    else {
      $and->condition('species_id', 1)
        ->condition('species_id', 0);
    }
  }
  if ($and->count() > 0) {
    $query->condition($and);
  }

  $species_str = isset($species) ? implode(';', $species) : "";
  $species_form = "<div class=\"row\"><div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-species\">Species:</label><br>"
        . "<input id=\"gttn-org-species\" type=\"textfield\" name=\"species\" value=\"{$species_str}\"></div>";
  $method = isset($methods) ? implode(';', $methods) : "";
  //method original; commented. Uncomment to roll back
  /*
  $method_form = "<div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-method\">Methods:</label><br>"
        . "<input id=\"gttn-org-method\" type=\"text\" name=\"method\" value=\"{$method}\"></div>";
  */
  //methods list
  //prepare methods select
  $all_methods_query = db_select('gttn_profile_method', 'm')
    ->distinct()
    ->fields('m', array('method_id', 'name'))
    ->execute();

  $method_form = "<div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-method\">Methods:</label><br>"
        . "<select id=\"gttn-org-method\" class=\"form-select\" name=\"method\" multiple>"
        ."<option value=\"0\">- Select -</option>";
  while (($method = $all_methods_query->fetchObject())) {
    $method_form.="<option value=".$method->name;
    if ( isset($methods)&&in_array($method->name,$methods))
        $method_form.=" selected";
    $method_form.=">".$method->name."</option>";
  }
  $method_form.="</select></div>";

  $product = isset($products) ? implode(';', $products) : "";
  $product_form = "<div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-product\">Products:</label><br>"
        . "<input id=\"gttn-org-product\" type=\"text\" name=\"product\" value=\"{$product}\"></div>";
  $region = isset($regions) ? implode(';', $regions) : "";
  $region_form = "<div class=\"col-md-3\" style=\"display:inline-block;\"><label for=\"gttn-org-region\">Regions:</label><br>"
        . "<input id=\"gttn-org-region\" type=\"text\" name=\"region\" value=\"{$region}\"></div></div>";
  $search_button = "<input style=\"margin-top:20px;margin-bottom:20px;\" type=\"submit\" id=\"gttn-org-search\" class=\"form-submit\" value=\"Search\">";

  $search_form = $species_form . $method_form . $product_form . $region_form . $search_button;
  $species_img = " <img src=\"$base_url/sites/all/modules/gttn_profile/images/verified.png\" title=\"This organization has proven that they are capable of analyzing this species\">";
  $method_img = " <img src=\"$base_url/sites/all/modules/gttn_profile/images/verified.png\" title=\"This organization has proven that they are capable of analyzing this method\">";

  $query = $query->execute();

  $rows = array();
  while (($org_id = $query->fetchObject())) {
    $result = db_select('gttn_profile_organization', 'o')
      ->fields('o')
      ->condition('organization_id', $org_id->organization_id)
      ->range(0, 1)
      ->execute()->fetchObject();
    $org_id = $result->organization_id;
    $name = $result->name;

    if (gttn_profile_access('browse_organization_details', $org_id)) {
      $name = "<a href=\"$org_id\">$name</a>";
    }

    $ref = $result->website;
    if (substr($ref, 0, 4) != 'http') {
      $ref = "http://$ref";
    }
    if (valid_url($ref, TRUE)) {
      $site = "<a target=\"blank\" href=\"{$ref}\">{$result->website}</a>";
    }
    else {
      $site = $result->website;
    }

    $species = implode(", ", array_map(
      function ($data) use ($species_img, &$org_id) {
        $result = "{$data->genus} {$data->species}";
        $result .= gttn_profile_verify_organization($org_id, $data->organism_id, 'species') ? $species_img : "";
        return $result;
      },
      gttn_profile_organization_get_species($org_id)
    ));

    $methods = array();
    foreach (gttn_profile_organization_get_methods($org_id) as $method) {
      $methods[] = $method->name . (gttn_profile_verify_organization($org_id, $method->name, 'method') ? $method_img : "");
    }
    $methods = implode(", ", $methods);
    $products = array();
    foreach (gttn_profile_organization_get_products($org_id) as $product) {
      $products[] = $product->name;
    }
    $products = implode(", ", $products);
    $regions = array();
    foreach (gttn_profile_organization_get_regions($org_id) as $region) {
      $regions[] = $region->name;
    }
    $regions = implode(", ", $regions);

    $row = array($name, $site, $species, $methods, $products, $regions);
    if (gttn_profile_access('edit_organization', $org_id)) {
      $row[] = "<a href=\"edit/$org_id\">Edit Organization</a>";
    }
    $rows[$name] = $row;
  }

  ksort($rows);

  $current_page = pager_default_initialize(count($rows), $per_page);
  $chunks = array_chunk($rows, $per_page, TRUE);

  $vars = array(
    'header' => array(
      'Name', 'Website', 'Supported species', 'Supported methods', 'Supported products', 'Supported regions',
    ),
    'rows' => isset($chunks[$current_page]) ? $chunks[$current_page] : NULL,
    'attributes' => array('class' => array('view'), 'id' => 'gttn_profile_table_display'),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => '',
  );

  $output = theme_table($vars);

  $output = $search_form . theme('pager', array('quantity', count($rows))) . $output;
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  drupal_add_css(drupal_get_path('module', 'gttn_profile') . '/css/style.css');
  return $output;
}

/**
 *
 */
function gttn_profile_verify_organization($org_id, $record, $type) {
  if (!module_exists('gttn_tpps')) {
    return FALSE;
  }
  switch ($type) {
    case 'species':
      $query = db_select('gttn_tpps_organization_inventory', 'oi');
      $query->join('chado.stock', 's', 's.stock_id = oi.sample_id');
      $query->fields('oi');
      $query->condition('oi.organization_id', $org_id);
      $query->condition('oi.exists', 1);
      $query->condition('s.organism_id', $record);
      $query->range(0, 1);
      $query = $query->execute();
      return (bool) $query->rowCount();

    case 'method':
      $query = db_select('gttn_tpps_organization_project', 'op');
      $query->fields('op');
      switch ($record) {
        case 'SNPs':
          // TODO:
          // We should be actually submitting SNP data and checking for it in the genotype table.
          $term = chado_get_cvterm(array(
            'name' => 'SNPs source',
            'is_obsolete' => 0,
          ))->cvterm_id;
          $query->join('chado.projectprop', 'pp', 'pp.project_id = op.project_id');
          $query->condition('pp.type_id', $term);
          break;

        case 'SSRs':
          $term = chado_get_cvterm(array(
            'name' => 'microsatellite',
            'cv_id' => array(
              'name' => 'sequence',
            ),
            'is_obsolete' => 0,
          ))->cvterm_id;
          $query->join('chado.genotype_call', 'gc', 'gc.project_id = op.project_id');
          $query->join('chado.genotype', 'g', 'gc.genotype_id = g.genotype_id');
          $query->condition('g.type_id', $term);
          break;

        case 'Isotope':
          $term = chado_get_cvterm(array(
            'name' => 'Isotope',
            'cv_id' => array(
              'name' => 'ncit',
            ),
            'is_obsolete' => 0,
          ))->cvterm_id;
        case 'DART':
          if ($record != 'Isotope') {
            $term = chado_get_cvterm(array(
              'name' => 'direct analysis in real time',
              'cv_id' => array(
                'name' => 'chmo',
              ),
              'is_obsolete' => 0,
            ))->cvterm_id;
          }
          $query->join('chado.project_stock', 'ps', 'ps.project_id = op.project_id');
          $query->join('chado.stock_phenotype', 'sp', 'sp.stock_id = ps.stock_id');
          $query->join('chado.phenotype', 'p', 'p.phenotype_id = sp.phenotype_id');
          $query->condition('p.attr_id', $term);
          break;

        case 'Anatomy':
          // TODO
          break;
      }
      $query->condition('op.organization_id', $org_id);
      $query->range(0, 1);
      $query = $query->execute();
      return (bool) $query->rowCount();
      break;

    case 'region':
      //TODO
      break;
  }
  return FALSE;
}

/**
 *
 */
function gttn_profile_organization_new_member($org_id, $uid, $contact_id, $is_primary = FALSE) {

  $and = db_and()
    ->condition('uid', $uid)
    ->condition('organization_id', $org_id);
  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition($and)
    ->execute();
  // If the user was already a member, then do nothing.
  if (($query->fetchObject())) {
    return;
  }

  gttn_profile_create_record('gttn_profile_organization_members', array(
    'contact_id' => $contact_id,
    'uid' => $uid,
    'organization_id' => $org_id,
    'is_primary_contact' => $is_primary,
    'status' => $is_primary ? 1 : NULL,
  ));

  if (!$is_primary) {
    gttn_profile_organization_notify($org_id, $uid, $contact_id, 'new_member');
  }
}

/**
 *
 */
function gttn_profile_organization_remove_member($org_id, $uid, $contact_id) {

  $and = db_and()
    ->condition('uid', $uid)
    ->condition('organization_id', $org_id);
  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition($and)
    ->execute();
  // If the user was not already a member, then do nothing.
  if (!($query->fetchObject())) {
    return;
  }

  db_delete('gttn_profile_organization_members')
    ->condition($and)
    ->execute();

  gttn_profile_organization_notify($org_id, $uid, $contact_id, 'remove_member');
}

/**
 *
 */
function gttn_profile_organization_notify($org_id, $uid, $contact_id, $type) {

  $and = db_and()
    ->condition('organization_id', $org_id)
    ->condition('is_primary_contact', TRUE);
  $primary_id = db_select('gttn_profile_organization_members', 'm')
    ->fields('m', array('uid'))
    ->condition($and)
    ->execute()->fetchObject()->uid ?? NULL;

  if (empty($primary_id)) {
    return;
  }

  if ($type == 'new_member') {
    $subject = 'GTTN: Pending membership claim';
  }
  if ($type == 'remove_member') {
    $subject = 'GTTN: Organization member removed';
  }

  $primary = user_load($primary_id);
  $to = $primary->mail;
  $lang = user_preferred_language($primary);
  $params = array(
    'subject' => $subject,
    'org_id' => $org_id,
    'primary_contact' => $primary,
    'user' => user_load($uid),
    'user_contact_id' => $contact_id,
  );
  $from = variable_get('site_mail');

  drupal_mail('gttn_profile', $type, $to, $lang, $params, $from, TRUE);
}

/**
 *
 */
function gttn_profile_organization_approve_member($uid, $org_id) {

  global $user;

  if ($user->uid == 0) {
    $dest = drupal_get_destination();
    drupal_goto('user/login', array('query' => $dest));
  }
  elseif (!gttn_profile_access('approve_organization_members', $org_id)) {
    drupal_access_denied();
    return;
  }

  $and = db_and()
    ->condition('uid', $uid)
    ->condition('organization_id', $org_id);
  $query = db_update('gttn_profile_organization_members')
    ->fields(array(
      'status' => 1,
    ))
    ->condition($and)
    ->execute();

  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');
  $output = "";
  drupal_set_message(t('Congratulations, the user has been approved!'));
  return drupal_render($output);
}

/**
 *
 */
function gttn_profile_organization_load($org_ids) {
  $results = array();
  $result_array = TRUE;
  if (!is_array($org_ids)) {
    $org_ids = array($org_ids);
    $result_array = FALSE;
  }

  $query = db_select('gttn_profile_organization', 'o')
    ->fields('o')
    ->condition('organization_id', $org_ids, 'IN')
    ->execute();

  while (($org = $query->fetchObject())) {
    $results[] = $org;
  }

  if (!$result_array) {
    $results = current($results);
  }
  return $results;
}

/**
 *
 */
function gttn_profile_is_primary_contact($org_id, $user) {
  if (!is_object($user)) {
    $user = user_load($user);
  }
  $query = db_select('gttn_profile_organization_members', 'm')
    ->fields('m')
    ->condition('organization_id', $org_id)
    ->condition('uid', $user->uid)
    ->condition('is_primary_contact', TRUE)
    ->condition('status', 1)
    ->range(0, 1)
    ->execute();

  return (bool) $query->fetchObject();
}
