<?php

function gttn_profile_sample_filter($form, $form_state){

  $samples = array();

  $and = db_and();

  if (!empty($form_state['values']['species'])){
    $species = explode(' ', $form_state['values']['species']);
    $species_and = db_and()
      ->condition('genus', $species[0])
      ->condition('species', $species[1]);

    $species_id = db_select('chado.organism', 'o')
      ->fields('o', array('organism_id'))
      ->condition($species_and)
      ->execute()->fetchObject()->organism_id;
    $and->condition('organism_id', $species_id);
  }
  $query = db_select('chado.stock','s')
    ->distinct();
  $query->fields('s', array('stock_id'));
  //samples only
  //Get cvterm_id
  $sample_cvt = chado_get_cvterm(array(
    'name' => 'biological sample',
    'cv_id' => array(
      'name' => 'sep',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;

  $and->condition('s.type_id', $sample_cvt);
  //Tissue
  $tissue_cvt = chado_get_cvterm(array(
    'name' => 'Tissue',
    'cv_id' => array(
      'name' => 'ncit',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;


  if (!empty($form_state['values']['tissues'])) {
    $query->join('chado.stockprop', 'sp', 's.stock_id = sp.stock_id');
    $and->condition('sp.type_id',$tissue_cvt);
    $and->condition('sp.value', $form_state['values']['tissues']);
  }
  //Type
  $type_cvt = chado_get_cvterm(array(
    'name' => 'sample type',
    'is_obsolete' => 0,
  ))->cvterm_id;

  if (!empty($form_state['values']['types'])) {
    $query->join('chado.stockprop', 'spp', 's.stock_id = spp.stock_id');
    $and->condition('spp.type_id',$type_cvt);
    $and->condition('spp.value', $form_state['values']['types']);
  }

  /*
    if (!empty($form_state['values']['regions']))
        $and->condition('region_id', $form_state['values']['regions']);
    if (!empty($form_state['values']['products']))
        $and->condition('product_id', $form_state['values']['products']);
    */

  if ($and->count() > 0){
    $query->condition($and);
  }

  $results = $query->execute();
  while (($result = $results->fetchObject())){
    $samples[] = $result->stock_id;
  }

  return $samples;
}

function gttn_profile_sample_candidates(){

  $per_page = 20;
  $rows = array();

  $args = drupal_get_query_parameters();
  if (!empty($args["samples"])) {
    $stocks = explode(';', $args["samples"]);
    if (empty($stocks) or !is_array($stocks)) {
      drupal_goto('sample_request');
    }
  }
  else {
    drupal_goto('sample_request');
  }

  //Get id's of properties
  //collection date
  $date_cvt = chado_get_cvterm(array(
    'name' => 'Collection Date',
    'cv_id' => array(
      'name' => 'ncit',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;
  //latitude
  $lat_cvt = chado_get_cvterm(array(
    'cv_id' => array(
      'name' => 'sio',
    ),
    'name' => 'latitude',
    'is_obsolete' => 0,
  ))->cvterm_id;
  //longitude
  $long_cvt = chado_get_cvterm(array(
    'cv_id' => array(
      'name' => 'sio',
    ),
    'name' => 'longitude',
    'is_obsolete' => 0,
  ))->cvterm_id;
  //country
  $country_cvt = chado_get_cvterm(array(
    'cv_id' => array(
      'name' => 'tripal_contact',
    ),
    'name' => 'Country',
    'is_obsolete' => 0,
  ))->cvterm_id;
  //collector
  $collector_cvt = chado_get_cvterm(array(
    'name' => 'specimen collector',
    'cv_id' => array(
      'name' => 'obi',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;
  //type
  $type_cvt = chado_get_cvterm(array(
    'name' => 'sample type',
    'is_obsolete' => 0,
  ))->cvterm_id;
  //analyzed
  $analyzed_cvt = chado_get_cvterm(array(
    'name' => 'sample analyzed',
    'is_obsolete' => 0,
  ))->cvterm_id;
  //storage
  $storage_cvt = chado_get_cvterm(array(
    'name' => 'storage location',
    'is_obsolete' => 0,
  ))->cvterm_id;
  //Tissue
  $tissue_cvt = chado_get_cvterm(array(
    'name' => 'Tissue',
    'cv_id' => array(
      'name' => 'ncit',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;
  //Collection method
  $method_cvt = chado_get_cvterm(array(
    'name' => 'Biospecimen Collection Method',
    'cv_id' => array(
      'name' => 'ncit',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;
  //shareable
  $share_cvt = chado_get_cvterm(array(
    'name' => 'shareable',
    'is_obsolete' => 0,
  ))->cvterm_id;
  $dimension_cvt = chado_get_cvterm(array(
    'name' => 'Dimension',
    'cv_id' => array(
      'name' => 'ncit',
    ),
    'is_obsolete' => 0,
  ))->cvterm_id;
  $or = db_or();
  foreach ($stocks as $stock_id) {
    $species_id = db_select('chado.stock', 'o')
      ->fields('o', array('organism_id'))
      ->condition('stock_id',$stock_id)
      ->execute()->fetchObject()->organism_id;
    $query = db_select('chado.organism', 'o')
      ->fields('o', array('genus','species'))
      ->condition('organism_id',$species_id)
      ->execute();
    while (($val = $query->fetchObject())){
      $spname = $val->genus.' '.$val->species;
    }

    //stock properties

    $lon = '';
    $lat = '';
    $country = '';
    $col_date = '';
    $data_type = '';
    $col_col = '';
    $analyzed = '';
    $storage = '';
    $tissue = '';
    $method = '';
    $share = '';
    $dimension ='';

    //Querying:
    //longitude
    $lon = gttn_profile_query_stock($stock_id,$long_cvt);
    //latitude
    $lat = gttn_profile_query_stock($stock_id,$lat_cvt);
      //country
    $country = gttn_profile_query_stock($stock_id,$country_cvt);
    //collection date
    $col_date = gttn_profile_query_stock($stock_id,$date_cvt);
      //Data type
    $data_type = gttn_profile_query_stock($stock_id,$type_cvt);
      //collector
    $col_col = gttn_profile_query_stock($stock_id,$collector_cvt);
      //storage
    $storage = gttn_profile_query_stock($stock_id,$storage_cvt);
      //analyzed
    $analyzed = gttn_profile_query_stock($stock_id,$analyzed_cvt);
      //tissue
    $tissue = gttn_profile_query_stock($stock_id,$tissue_cvt);
    //Method
    $method = gttn_profile_query_stock($stock_id,$method_cvt);
    
    //share
    $sharein = gttn_profile_query_stock($stock_id,$share_cvt);
    if($sharein=='') $share = $sharein;
    else $share = $sharein == '1' ? 'yes' : 'no';
    //dimensions
    $dimension = gttn_profile_query_stock($stock_id,$dimension_cvt);
    $fordetails = 'Xylarium ID: '.''.'\nCollector: '.$col_col.'\nCollection date: '.$col_date;
    $fordetails .= '\nCountry: '.$country.'\nLongitude: '.$lon.'\nLatitude: '.$lat;
    $clickme = "<input id=\"details_button\" type=\"button\" onClick=\"alert('$fordetails','status')\"  value=\"More...\"></input>";
    //Prepare table
    $row = array($stock_id,$spname,$data_type,$tissue,$dimension,$method,$storage,$analyzed,$share,$clickme);
    $rows[$stock_id] = $row;
  }
  ksort($rows);

  $current_page = pager_default_initialize(count($rows), $per_page);
  $chunks = array_chunk($rows, $per_page, TRUE);

  $vars = array(
    'header' => array(
      'Internal Sampe ID','species', 'type','Tissue','Size','Method','Storage','Analyzed','Shareable','Details'
    ),
    'rows' => isset($chunks[$current_page]) ? $chunks[$current_page] : NULL,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => NULL,
    'sticky' => FALSE,
    'empty' => ''
  );

  $output = theme_table($vars);

  $output = theme('pager', array('quantity', count($rows))) . $output;
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/gttn_profile.js');
  drupal_add_js(drupal_get_path('module', 'gttn_profile') . '/js/block_elastic.js');

  return $output;
}
//return stock property
function gttn_profile_query_stock($id_stock,$id_prop) {
  $retval = '';
  $query = db_select('chado.stockprop', 'o')
        ->fields('o', array('value'))
        ->condition('stock_id',$id_stock)
    ->condition('type_id',$id_prop)
        ->execute();
  while (($val = $query->fetchObject())){
    $retval = $val->value;
  }
  return $retval;
}
//display drupal message
function gttn_profile_message($message) {
  drupal_set_message(t($message), 'status');
  return true;
}