<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_efi_login($email, $password) {
  $api_end = 'login.php';

  $fields = array(
    'email' => $email,
    'password' => $password,
  );
  $result = gttn_profile_efi_query($api_end, $fields);

  if (!$result or !isset($result->loginSuccess) or !$result->loginSuccess) {
    return FALSE;
  }

  return $result;
}

/**
 * WIP
 */
function gttn_profile_efi_reset_password($email) {
  $api_end = 'reset-password.php';

  $fields = array(
    'email' => $email,
    'requestType' => 'token',
  );

  $result = gttn_profile_efi_query($api_end, $fields);

  if (!$result or !isset($result->status) or !$result->status or !isset($result->tokenSent) or !$result->tokenSent) {
    return FALSE;
  }

  return TRUE;
}

/**
 *
 */
function gttn_profile_efi_load_organization($org_id, $email, $password) {
  $org = db_select('gttn_profile_organization', 'o')
    ->fields('o', array('organization_id'))
    ->condition('efi_id', $org_id)
    ->range(0, 1)
    ->execute()->fetchObject();
  
  if (!empty($org)) {
    return gttn_profile_organization_load($org->organization_id);
  }

  $api_end = 'get-laboratory.php';

  $fields = array(
    'laboratory' => $org_id,
    'email' => $email,
    'password' => $password,
  );

  $result = gttn_profile_efi_query($api_end, $fields);

  if (!$result or !isset($result->status) or !$result->status) {
    return FALSE;
  }

  $org_name = $result->laboratory[0]->organisation;
  $org = db_select('gttn_profile_organization', 'o')
    ->fields('o', array('organization_id'))
    ->condition('name', $org_name)
    ->range(0, 1)
    ->execute()->fetchObject();

  if (!empty($org)) {
    db_update('gttn_profile_organization')
      ->fields(array(
        'efi_id' => $org_id,
      ))
      ->condition('organization_id', $org->organization_id)
      ->execute();
    return gttn_profile_organization_load($org->organization_id);
  }

  $org_record = array(
    'name' => $result->laboratory[0]->organisation,
    'address_1' => $result->laboratory[0]->address_1,
    'address_2' => $result->laboratory[0]->address_2,
    'country' => gttn_profile_efi_country($result->laboratory[0]->country),
    'city' => $result->laboratory[0]->city,
    'region' => $result->laboratory[0]->region,
    'postal_code' => $result->laboratory[0]->postal_code,
    'website' => $result->laboratory[0]->website,
  );

  $org = gttn_profile_create_record('gttn_profile_organization', $org_record);

  return gttn_profile_organization_load($org);
}

/**
 *
 */
function gttn_profile_efi_country($country_id) {
  // TODO.
  return '';
}

/**
 *
 */
function gttn_profile_efi_query($location, $fields) {
  $url = 'https://form.spd.globaltimbertrackingnetwork.org/api/' . $location;

  $field_string = $fields;
  if (gettype($fields) == 'array' or gettype($fields) == 'object') {
    $field_string = http_build_query($fields);
  }

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $result = curl_exec($ch);
  $result = json_decode($result);

  return $result;
}

/**
 *
 */
function gttn_profile_efi_sync($options = array()) {
  $token = variable_get('gttn_profile_efi_sync_token', NULL);
  $api_end = 'process-uconn-refdb.php';

  if (empty($token)) {
    return FALSE;
  }

  if (empty($options)) {
    return FALSE;
  }

  $organization = gttn_profile_organization_load($options['org_id']);

  if (empty($organization->efi_id)) {
    $org_id = $options['org_id'];
    $name = $organization->name ?? "unknown name";
    watchdog('gttn_profile', "Could not find matching EFI ID for organization \"$name\" (ID: $org_id), so could not properly sync EFI information.", array(), WATCHDOG_WARNING);
    return FALSE;
  }

  $capabilities = array(
    'methods' => array(),
    'supportedSpecies' => array(),
  );

  if (!empty($options['methods'])) {
    foreach ($options['methods'] as $method) {
      if (!empty($method)) {
        if (strlen($method) > 15) {
          $method = trim(substr($method, 0, strlen($method) - 15));
        }
        $capabilities['methods'][] = $method;
      }
    }
  }

  if (!empty($options['species'])) {
    foreach ($options['species'] as $species) {
      $species_object = new stdClass();
      $species_object->genus = $species['genus'];
      $species_object->species = $species['species'];
      $capabilities['supportedSpecies'][] = $species_object;
    }
  }

  $payload = json_encode(array(
    'token' => $token,
    'laboratory_id' => $organization->efi_id,
    'capabilities' => $capabilities,
  ));

  $result = gttn_profile_efi_query($api_end, $payload);

  return (!empty($result->updateSuccess) and $result->updateSuccess);
}
