<?php

/**
 * @file
 */

/**
 *
 */
function gttn_profile_user_chado_update(&$edit, &$account) {
  if ($account->uid != 0) {
    $uid = $account->uid;
  }
  else {
    return;
  }

  if ($account->is_new) {
    $contact_id = gttn_profile_create_record('chado.contact', array(
      'type_id' => array(
        'name' => 'Person',
        'cv_id' => array(
          'name' => 'tripal_contact',
        ),
      ),
      'name' => $edit['full_name'],
    ));
    gttn_profile_create_record('gttn_profile_user_chado', array(
      'uid' => $uid,
      'contact_id' => $contact_id,
    ));
    gttn_profile_create_record('chado.contactprop', array(
      'contact_id' => $contact_id,
      'type_id' => array(
        'name' => 'email address',
        'cv_id' => array(
          'name' => 'SIO',
        ),
      ),
      'value' => $account->mail,
    ));
  }
  else {
    $contact_id = db_select('gttn_profile_user_chado', 'u')
      ->fields('u', array('contact_id'))
      ->condition('uid', $uid)
      ->execute()->fetchObject()->contact_id;
  }

  foreach ($edit['org'] as $org_id => $selected) {
    if (!empty($selected)) {
      $is_efi = !empty($edit['efi']);
      gttn_profile_organization_new_member($org_id, $uid, $contact_id, $is_efi);
    }
    else {
      gttn_profile_organization_remove_member($org_id, $uid, $contact_id);
    }
  }
}

/**
 * Implements hook_user_load().
 */
function gttn_profile_user_load($users) {
  $person_cvt = chado_get_cvterm(array(
    'name' => 'Person',
    'cv_id' => array(
      'name' => 'tripal_contact',
    ),
  ))->cvterm_id;

  $contacts = db_select('gttn_profile_user_chado', 'u');
  $contacts->join('chado.contact', 'c', "c.contact_id = u.contact_id and c.type_id = $person_cvt");
  $contacts->fields('c');
  $contacts->fields('u', array('uid'));
  $contacts->condition('u.uid', array_keys($users), 'IN');
  $contacts = $contacts->execute();

  while (($contact = $contacts->fetchObject())) {
    $users[$contact->uid]->chado_record = $contact;
    unset($users[$contact->uid]->chado_record->uid);
  }

  foreach ($users as $uid => $user) {
    $users[$uid]->organizations = array();
    $users[$uid]->organization_status = array();

    $query = db_select('gttn_profile_organization_members', 'm')
      ->fields('m', array('organization_id', 'status'))
      ->condition('uid', $uid)
      ->execute();

    while (($result = $query->fetchObject())) {
      $users[$uid]->organizations[] = $result->organization_id;
      $users[$uid]->organization_status[$result->organization_id] = $result->status;
    }
  }
}

/**
 *
 */
function gttn_profile_authentication_check_user($email, $password) {
  try {
    $result = gttn_profile_efi_login($email, $password);

    if ($result) {
      $info = new stdClass();
      $info->name = $email;
      $info->mail = $email;
      $info->full_name = "{$result->givenName} {$result->familyName}";
      $info->org = $result->laboratoryId;
      return $info;
    }
    return FALSE;
  }
  catch (Exception $e) {
    watchdog('gttn_profile', 'Error %error_message.', array('%error_message' => $e->getMessage()), WATCHDOG_NOTICE);
    return FALSE;
  }
}
