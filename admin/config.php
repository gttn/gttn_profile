<?php

/**
 * @file
 */

/**
 * Creates admin settings form for gttn_profile.
 *
 * @param array $form
 *   The form to be populated.
 * @param array $form_state
 *   The state of the form to be populated.
 *
 * @return array The populated form.
 */
function gttn_profile_admin_settings($form, &$form_state) {

  $form['gttn_profile_efi_sync_token'] = array(
    '#type' => 'textfield',
    '#title' => t('EFI Sync Token'),
    '#description' => t('Token to work with EFI SPD sync. Probably need to ask Simo to generate this for us.'),
    '#default_value' => variable_get('gttn_profile_efi_sync_token', NULL),
  );

  return system_settings_form($form);
}
